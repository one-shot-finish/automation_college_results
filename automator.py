#!/usr/bin/env python

'''
Created on 22-Jul-2017

@author: don
'''

from selenium import webdriver
import pandas as pd
from BeautifulSoup import BeautifulSoup

class Automator:

	def __init__(self, config_dict):
		self.config_dict = config_dict
		self.url = self.config_dict['url']
		self.chrome_driver_path = self.config_dict['chrome_driver_path']
		self.xpath_reg_no = self.config_dict['xpath_reg_no']
		self.xpath_submit = self.config_dict['xpath_submit']
		self.labels = self.config_dict['labels']
		self.output_file = self.config_dict['output_file']
		pass

	def get_tuple(self, res):
		return (x for x in res)
		
	def parse_result(self, reg_no, soup):
		fail_cnt = 0
		user_data, user_data_saved = '', ''
		s = list()
		for rec in soup.findAll('tr'):
			my_str = rec.text
			if 10 < len(my_str):
				if my_str[-4:] == 'PASS' or my_str[-4:] == 'FAIL':
					if my_str[-4:] == 'PASS':
						gr = -1
						if my_str[-6:-4] == '10':
							gr = 10
						else:
							gr = int(my_str[-5])
					else:
						gr = 0
						fail_cnt += 1
					s.append(gr)
			if len(my_str) > 5:
				if my_str[:3] == 'SEM':
					sgpa = my_str.index('SGPA')
					cgpa = my_str.index('CGPA')
					s.append(float(my_str[sgpa+4:cgpa]))
					s.append(float(my_str[cgpa+4:]))
			if len(my_str) > 20:
				if my_str[:20] == 'BRANCH WISE SEM RANK':
					bwsr = my_str.index('BRANCH WISE SEM RANK:')
					cwsr = my_str.index('COLLEGE WISE SEM RANK:')
					s.append(int(my_str[bwsr+21:cwsr]))
					s.append(int(my_str[cwsr+22:]))
			if len(my_str) > 23:
				if my_str[:23] == 'BRANCH WISE CUMM.. RANK':
					bwsr = my_str.index('BRANCH WISE CUMM.. RANK:')
					cwsr = my_str.index('COLLEGE WISE CUMM.. RANK:')
					s.append(int(my_str[bwsr+24:cwsr]))
					s.append(int(my_str[cwsr+25:]))
		if fail_cnt > 0:
			for i in range(4):
				s.append(-1)
		s.append(reg_no)
		return s

	def scrape_result(self, reg_no):
		
		browser = webdriver.Chrome(self.chrome_driver_path)
		browser.get(self.url)
		browser.find_element_by_xpath(self.xpath_reg_no).send_keys(reg_no)
		browser.find_element_by_xpath(self.xpath_submit).click()
		soup = BeautifulSoup(browser.page_source)
		
		s = self.parse_result(reg_no, soup)
		
		browser.close()
		print s	
		return s
	
	
	
	def run(self):
	
		rec_list = list()
		for i in range(1, 100):
			if i < 10:	
				res = self.scrape_result('14501A050'+str(i))
			else:
				res = self.scrape_result('14501A05'+str(i))
			rec_dict = self.get_tuple(res)
			rec_list.append(rec_dict)
		
		x = 'AB'	
		for j in x:
			for i in range(10):
				res = self.scrape_result('14501A05'+j+str(i))
				rec_dict = self.get_tuple(res)
				rec_list.append(rec_dict)
		res = self.scrape_result('14501A05C0')
		rec_list.append(self.get_tuple(res))


		for i in range(1,25):
			if i < 10:
				res = self.scrape_result('15505A050'+str(i))
			else:
				res = self.scrape_result('15505A05'+str(i))
			rec_dict = self.get_tuple(res)
			rec_list.append(rec_dict)
		
		labels = [x.strip(",'") for x in self.labels.strip('[]').split(' ')]
		df = pd.DataFrame.from_records(rec_list, columns=labels)

		df.to_csv(self.output_file)
